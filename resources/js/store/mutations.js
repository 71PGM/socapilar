export default {
    setCodeAccess(state, code) {
        state.code = code
    },
    setUsers(state, users) {
        state.users = users
    }
}