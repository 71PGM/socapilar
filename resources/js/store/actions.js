export default {
    setCodeAccess({commit}, code) {
        commit('setCodeAccess', code)
    },
    setUsers({commit}, users) {
        commit('setUsers', users)
    }
}