<?php

namespace App\Http\Controllers;

use VK\Client\VKApiClient;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

class SocApiController extends Controller
{
    private $vkClient;
    private $oauth;
    private $client_id;
    private $redirect_uri;
    private $client_secret;

    public function __construct()
    {
        $this->vkClient = new VKApiClient();
        $this->oauth = new VKOAuth();
        $this->client_id = getenv('VK_ID');
        $this->redirect_uri = getenv('VK_URL_REDIRECT');
        $this->client_secret = getenv('VK_SECRET_KEY');
    }

    public function index()
    {
        return view('basic');
    }

    public function accessKey()
    {
        return response()->json($this->getUrl());
    }

    public function oauth()
    {
        return redirect('/soc-api?code=' . request()->input('code'));
    }

    public function socApi()
    {
        return view('soc_api');
    }

    public function getDataUser($userId)
    {
        $code = request()->input('code');

        $response = $this->oauth->getAccessToken($this->client_id, $this->client_secret, $this->redirect_uri, $code);
        $access_token = $response['access_token'];

        return response()->json([
                        'user' => $this->vkClient->users()->get($access_token, ['fields' => $this->dataUser()]),
                        'friends' => $this->vkClient->friends()->get($access_token, ['user_ids' => [$userId], 'fields' => $this->getDataFriends(),]),
                        //'likes' => $this->vkClient->likes()->getList($access_token, $this->dataLikes($userId)),
                ]);
    }

    private function dataLikes($userId)
    {
        $type = 'type';

        return [
            $type => 'post',
            $type => 'comment',
            $type => 'photo',
            $type => 'audio',
            $type => 'video',
            $type => 'note',
            $type => 'market',
            $type => 'photo_comment',
            $type => 'video_comment',
            $type => 'topic_comment',
            $type => 'market_comment',
            $type => 'sitepage',
            'owner_id' => $userId
        ];
    }

    private function getUrl()
    {
        $display = VKOAuthDisplay::PAGE;
        $scope = [VKOAuthUserScope::WALL, VKOAuthUserScope::GROUPS];

        return $this->oauth->getAuthorizeUrl(VKOAuthResponseType::CODE, $this->client_id, $this->redirect_uri, $display, $scope, $this->client_secret);
    }

    private function dataUser()
    {
        return [
            'photo_id',
            'verified',
            'sex',
            'bdate',
            'city',
            'country',
            'home_town',
            'has_photo',
            'photo_50',
            'photo_100',
            'photo_200_orig',
            'photo_200',
            'photo_400_orig',
            'photo_max',
            'photo_max_orig',
            'online',
            'domain',
            'has_mobile',
            'contacts',
            'site',
            'education',
            'universities',
            'schools',
            'status',
            'last_seen',
            'followers_count',
            'common_count',
            'occupation',
            'ickname',
            'relatives',
            'relation',
            'personal',
            'connections',
            'exports',
            'activities',
            'interests',
            'music',
            'movies',
            'tv',
            'books',
            'games',
            'about',
            'quotes',
            'can_post',
            'can_see_all_posts',
            'can_see_audio',
            'can_write_private_message',
            'can_send_friend_request',
            'is_favorite',
            'is_hidden_from_feed',
            'timezone',
            'screen_name',
            'maiden_name',
            'crop_photo',
            'is_friend',
            'friend_status',
            'career',
            'military',
            'blacklisted',
            'blacklisted_by_me'
        ];
    }

    private function getDataFriends()
    {
        return [
            'nickname',
            'domain',
            'sex',
            'bdate',
            'city',
            'country',
            'timezone',
            'photo_50',
            'photo_100',
            'photo_200_orig',
            'has_mobile',
            'contacts',
            'education',
            'online',
            'relation',
            'last_seen',
            'status',
            'can_write_private_message',
            'can_see_all_posts',
            'can_post',
            'universities'
        ];
    }
}
