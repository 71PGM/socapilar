<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/access-key', 'SocApiController@accessKey');
Route::get('/oauth', 'SocApiController@oauth');
Route::post('/get/user/{userId}', 'SocApiController@getDataUser');
